package com.tsystems.javaschool.tasks.calculator;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     *                  parentheses, operations signs '+', '-', '*', '/'<br>
     *                  Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    private StringBuilder outputArray = new StringBuilder();
    private List<String> operationStack = new ArrayList<>();

    private boolean transformStackAndArray(int param){
        switch (param) {
            case 1:
                if (operationStack.size() > 1){
                    int lastIndex = operationStack.size() - 1;

                    String last = operationStack.get(lastIndex);
                    String penult = operationStack.get(lastIndex - 1);

                    if(last.equals("+") || last.equals("-")){
                        //System.out.println(operationStack.toString());
                        while(operationStack.size() > 1 && !penult.equals("(")){
                            --lastIndex;
                            outputArray.append(penult + " ");
                            operationStack.remove(lastIndex--);
                            if(lastIndex > -1) {
                                penult = operationStack.get(lastIndex);
                            }
                        }

                    } else if(last.equals("*") || last.equals("/")){
                        if(penult.equals("*") || penult.equals("/")){
                            outputArray.append(penult + " ");
                            operationStack.remove(operationStack.size() - 2);
                        }

                    } else if(last.equals(")")){
                        operationStack.remove(lastIndex--);
                        last = operationStack.get(lastIndex);

                        while(operationStack.size() > 0 && !last.equals("(")){
                            outputArray.append(last + " ");
                            operationStack.remove(lastIndex--);
                            if(lastIndex == -1){
                                return false; // ERROR :: ...), no opening bracket
                            }
                            last = operationStack.get(lastIndex);
                        }
                        operationStack.remove(lastIndex);
                    }
                }
                break;
            default:
                if(operationStack.size() > 0){
                    int lastIndex = operationStack.size() - 1;
                    while(operationStack.size() > 0) {
                        if (operationStack.get(lastIndex).equals("(")) {
                            return false; // ERROR :: (... , no closing bracket
                        }
                        outputArray.append(operationStack.get(lastIndex) + " ");
                        operationStack.remove(lastIndex--);
                    }

                }
        }
        return true; // OK
    }

    private String makeRPN(String statement){
        statement = statement.replaceAll("[()+*/-]", " $0 ");

        String[] words = statement.split(" ");

        for(String w: words){
            if(w.matches("[()+/*-]+")){
                operationStack.add(w);
                if(!transformStackAndArray(1)){
                    return null;
                }
            } else if(w.matches("[0-9.]+")){
                outputArray.append(w + " ");
            }


        }

        if(!transformStackAndArray(0)){
            return null;
        }
        return outputArray.toString();
    }

    public String evaluate(String statement) {
        // TODO: Implement the logic here
        if(statement == null || !statement.matches("[0-9+()*/.-]+")){
            return null;
        }

        String strRPN = makeRPN(statement);
        if(strRPN == null){
            return null;
        }

        List<Double> resultStack = new ArrayList<>();

        for(String s: strRPN.split(" ")){
            if(s.matches("[0-9.]+")){
                try {
                    Double d = Double.parseDouble(s);
                    resultStack.add(d);
                } catch (NumberFormatException e){
                    return null;
                }
            } else if(s.matches("[+/*-]")){
                if(resultStack.size() < 2){
                    return null;
                }
                int lastIndex = resultStack.size() - 1;
                Double last = resultStack.get(lastIndex);
                resultStack.remove(lastIndex--);
                if(s.equals("+")) {
                    resultStack.set(lastIndex, resultStack.get(lastIndex) + last);
                }else if(s.equals("-")){
                    resultStack.set(lastIndex, resultStack.get(lastIndex) - last);
                }else if(s.equals("*")){
                    resultStack.set(lastIndex, resultStack.get(lastIndex) * last);
                }else if(s.equals("/")){
                    if(last.equals(0d)){
                        return null;
                    }
                    resultStack.set(lastIndex, resultStack.get(lastIndex) / last);
                }
            }
        }
        if(resultStack.size() != 1){
            return null;
        }

        NumberFormat nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(4);
        return nf.format(resultStack.get(0)).replaceAll(",", ".");
    }
}
