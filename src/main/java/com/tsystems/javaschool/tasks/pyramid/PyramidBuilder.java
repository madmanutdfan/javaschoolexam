package com.tsystems.javaschool.tasks.pyramid;

import java.util.Comparator;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    private int rows = 0;
    private int columns = -1;

    private boolean canBuildPyramid(List<Integer> inputNumbers){
        if(inputNumbers == null || inputNumbers.size() == 0 || inputNumbers.size() > Integer.MIN_VALUE - 3){
            return false;
        }

        int pyramidSize = inputNumbers.size();
        int numToBuild = 0;

        while(numToBuild < pyramidSize){
            numToBuild += ++rows;
            columns += 2;
        }
        if(numToBuild != pyramidSize) {
            return false;
        }

        try{
            inputNumbers.sort(Comparator.naturalOrder());
        } catch (Exception e){
            return false;
        }

        return true;
    }

    public int[][] buildPyramid(List<Integer> inputNumbers) {
        // TODO : Implement your solution here

        if(!canBuildPyramid(inputNumbers)){
            throw new CannotBuildPyramidException();
        }

        int[][] pyramid = new int[rows][columns];
        int from = columns / 2;
        int r = 0; // rows
        int c = 0; // columns
        int i = 0; // inputNumbers index

        while(r < rows){
            int curNumInStr = r + 1; // number of numbers in a current string
            int curFrom = from--;    // start

            c = 0;
            while(c < columns){
                pyramid[r][c] = 0;

                if(c == curFrom && curNumInStr > 0) {
                    pyramid[r][c] = inputNumbers.get(i++);

                    curFrom += 2;
                    curNumInStr--;
                }
                c++;
            }
            r++;
        }

        return pyramid;
    }
}
